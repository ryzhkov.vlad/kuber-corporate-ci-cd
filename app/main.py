from fastapi import FastAPI
import os
import redis

app = FastAPI()


@app.get("/version")
def version():
    return {"version": 12}

@app.get("/")
def read_root():
    return {"Hello": "Hacker"}


@app.get("/high-load/{iter}")
def high_load(iter: int = 10000):
    for i in range(iter):
        iter * iter

@app.get("/redis")
def test_redis():
    redis_host = os.getenv('REDIS_HOST', 'unknown_redis_host')
    redis_port = int(os.getenv('REDIS_PORT', '6379'))
    res = {'redis_host': redis_host,
           'redis_port': redis_port}


    r = redis.StrictRedis(host=redis_host, port=redis_port)
    res['redis_connect'] = 'ok'
    r.set('a', 'abc')
    res['redis_set'] = 'ok'
    raw = r.get('a')
    res['got_raw'] = 'ok'
    v = raw.decode('utf-8')
    res['decode'] = 'ok'
    res['v'] = v

    return res
